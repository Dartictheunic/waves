﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;

public class AudioTest : MonoBehaviour
{
    public BezierSpline bezierSpline;
    public int samplesToSkip;
    public List<Vector3> pointsPositions;
    public List<Vector3> cleanPointsPositions;
    public List<float> tangent;
    public AnimationCurve aled;
    public Material testMat;
    public Texture2D PaintWaveformSpectrum(AudioClip audio, float saturation, int width, int height, Color col)
    {
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
        float[] samples = new float[audio.samples];
        float[] waveform = new float[width];
        audio.GetData(samples, 0);
        int packSize = (audio.samples / width) + 1;
        int s = 0;
        for (int i = 0; i < audio.samples; i += packSize)
        {
            waveform[s] = Mathf.Abs(samples[i]);
            s++;
        }

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                tex.SetPixel(x, y, Color.black);
            }
        }

        for (int x = 0; x < waveform.Length; x++)
        {
            if(x %16 == 0)
            {
            for (int y = 0; y <= waveform[x] * ((float)height * .75f); y++)
            {
                tex.SetPixel(x,y, col);
            }
            }
        }
        tex.Apply();

        return tex;
    }

    public void CreatePoints(AudioClip audio, int width, int height, Color col)
    {
        float[] samples = new float[audio.samples];
        float[] waveform = new float[width];
        audio.GetData(samples, 0);
        int packSize = (audio.samples / width) + 1;
        int s = 0;
        for (int i = 0; i < audio.samples; i += packSize)
        {
            waveform[s] = Mathf.Abs(samples[i]);
            s++;
        }

        pointsPositions = new List<Vector3>();

        for (int x = 0; x < waveform.Length; x++)
        {
            if (x % samplesToSkip == 0)
            {
                for (int y = 0; y <= waveform[x] * ((float)height * .75f); y++)
                {
                    pointsPositions.Add(new Vector3(x, y, 0));
                }
            }
        }

        CleanPositions();
    }

    public void CleanPositions()
    {
        cleanPointsPositions = new List<Vector3>();
        int i = 0;
        foreach (Vector3 vec in pointsPositions)
        {
            if (i < pointsPositions.Count - 1)
            {
                if (vec.x != pointsPositions[i + 1].x)
                {
                    cleanPointsPositions.Add(vec);
                }
            }

            i++;
        }

        DrawLines();

    }

    public void DrawLines()
    {
        int i = 0;
        foreach(Vector3 vec in cleanPointsPositions)
        {
            i++;
            aled.AddKey(vec.x / 1000, vec.y / 1000);
            var oskour = bezierSpline.InsertNewPointAt(i);
            oskour.position = vec;
        }
        
        tangent = new List<float>();
        foreach(Keyframe sexe in aled.keys)
        {
            tangent.Add(sexe.inTangent);
        }
    }

    private void Start()
    {
        CreatePoints(GetComponent<AudioSource>().clip, 5000, 500, Color.red);
        //testMat.mainTexture = PaintWaveformSpectrum(GetComponent<AudioSource>().clip, 1f, 1080, 1920, Color.red);
    }
}
