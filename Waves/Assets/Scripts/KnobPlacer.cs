﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BezierSolution;

public class KnobPlacer : MonoBehaviour
{
    [SerializeField]
    public KnobType knobType;
    private BezierPoint myPoint;

    void Start()
    {
        myPoint = GetComponentInParent<BezierPoint>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch(knobType)
        {
            case KnobType.left:
                {
                    transform.position = myPoint.precedingControlPointPosition;
                }break;

            case KnobType.right:
                {
                    transform.position = myPoint.followingControlPointPosition;
                }break;

            case KnobType.point:
                {
                    transform.position = myPoint.transform.position;
                }break;
        }
    }
}

public enum KnobType
{
    left,
    right,
    point
}
